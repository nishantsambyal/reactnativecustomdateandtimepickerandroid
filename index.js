import { NativeModules } from 'react-native';

const { CustomDateTimePickerDialog } = NativeModules;

export default CustomDateTimePickerDialog;
