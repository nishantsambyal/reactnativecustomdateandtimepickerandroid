package com.reactlibrary;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.Calendar;

public class CustomDateTimePickerDialogModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private int intHour, intMinute;

    public CustomDateTimePickerDialogModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "CustomDateTimePickerDialog";
    }

    @ReactMethod
    public void datePicker(final Callback successCallback) {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getCurrentActivity(), R.style.CustomDatePickerDialog,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        successCallback.invoke(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @ReactMethod
    public void timePicker(int timeInterval, final Callback successCallback) {
        CustomTimePickerDialog timePickerDialog = new CustomTimePickerDialog(timeInterval, getCurrentActivity(), new TimePickerDialog.OnTimeSetListener() {
            @SuppressLint("NewApi")
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String strTime = hourOfDay + ":" + minute + ":00";
                successCallback.invoke(strTime);
            }
        }, intHour, intMinute, false);
        timePickerDialog.show();

    }

}
